﻿namespace EBC_Calculator
{
    partial class GraphPanel
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.FormulaList = new System.Windows.Forms.ComboBox();
            this.PanelGraph = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.InputLevelJump = new System.Windows.Forms.TextBox();
            this.InputVerticalNode = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.InputValueStart = new System.Windows.Forms.TextBox();
            this.InputValueMax = new System.Windows.Forms.TextBox();
            this.InputLevels = new System.Windows.Forms.TextBox();
            this.ButtonGenerate = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.OutputBase = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.OutputIncr = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.PanelTooltips = new System.Windows.Forms.Panel();
            this.LabelTooltips = new System.Windows.Forms.Label();
            this.PanelGraph.SuspendLayout();
            this.panel2.SuspendLayout();
            this.PanelTooltips.SuspendLayout();
            this.SuspendLayout();
            // 
            // FormulaList
            // 
            this.FormulaList.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.FormulaList.FormattingEnabled = true;
            this.FormulaList.Items.AddRange(new object[] {
            "TestFormula",
            "LucidBeam",
            "DeathBlow"});
            this.FormulaList.Location = new System.Drawing.Point(12, 12);
            this.FormulaList.Name = "FormulaList";
            this.FormulaList.Size = new System.Drawing.Size(121, 21);
            this.FormulaList.TabIndex = 0;
            this.FormulaList.SelectedIndexChanged += new System.EventHandler(this.FormulaList_SelectedIndexChanged);
            // 
            // PanelGraph
            // 
            this.PanelGraph.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.PanelGraph.Controls.Add(this.panel2);
            this.PanelGraph.Location = new System.Drawing.Point(151, 0);
            this.PanelGraph.Name = "PanelGraph";
            this.PanelGraph.Size = new System.Drawing.Size(768, 400);
            this.PanelGraph.TabIndex = 1;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.SystemColors.Control;
            this.panel2.Controls.Add(this.InputLevelJump);
            this.panel2.Controls.Add(this.InputVerticalNode);
            this.panel2.Controls.Add(this.label9);
            this.panel2.Controls.Add(this.label10);
            this.panel2.Controls.Add(this.label8);
            this.panel2.Location = new System.Drawing.Point(599, 326);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(166, 71);
            this.panel2.TabIndex = 20;
            // 
            // InputLevelJump
            // 
            this.InputLevelJump.Location = new System.Drawing.Point(106, 19);
            this.InputLevelJump.MaxLength = 2;
            this.InputLevelJump.Name = "InputLevelJump";
            this.InputLevelJump.Size = new System.Drawing.Size(44, 20);
            this.InputLevelJump.TabIndex = 5;
            this.InputLevelJump.TabStop = false;
            this.InputLevelJump.Text = "1";
            this.InputLevelJump.TextChanged += new System.EventHandler(this.InputLevelJump_TextChanged_1);
            // 
            // InputVerticalNode
            // 
            this.InputVerticalNode.Location = new System.Drawing.Point(106, 45);
            this.InputVerticalNode.MaxLength = 2;
            this.InputVerticalNode.Name = "InputVerticalNode";
            this.InputVerticalNode.Size = new System.Drawing.Size(44, 20);
            this.InputVerticalNode.TabIndex = 6;
            this.InputVerticalNode.TabStop = false;
            this.InputVerticalNode.Text = "10";
            this.InputVerticalNode.TextChanged += new System.EventHandler(this.InputLevelJump_TextChanged_1);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(15, 9);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(43, 13);
            this.label9.TabIndex = 15;
            this.label9.Text = "Setting:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(15, 48);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(85, 13);
            this.label10.TabIndex = 18;
            this.label10.Text = "• Vertical Nodes";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(15, 22);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(66, 13);
            this.label8.TabIndex = 16;
            this.label8.Text = "• Level Skip";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 36);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(34, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Input:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 52);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(68, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "• Value Start";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 78);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(66, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "• Value Max";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 104);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(47, 13);
            this.label4.TabIndex = 5;
            this.label4.Text = "• Levels";
            // 
            // InputValueStart
            // 
            this.InputValueStart.Location = new System.Drawing.Point(89, 49);
            this.InputValueStart.MaxLength = 5;
            this.InputValueStart.Name = "InputValueStart";
            this.InputValueStart.Size = new System.Drawing.Size(44, 20);
            this.InputValueStart.TabIndex = 1;
            // 
            // InputValueMax
            // 
            this.InputValueMax.Location = new System.Drawing.Point(89, 75);
            this.InputValueMax.MaxLength = 5;
            this.InputValueMax.Name = "InputValueMax";
            this.InputValueMax.Size = new System.Drawing.Size(44, 20);
            this.InputValueMax.TabIndex = 2;
            // 
            // InputLevels
            // 
            this.InputLevels.Location = new System.Drawing.Point(89, 101);
            this.InputLevels.MaxLength = 2;
            this.InputLevels.Name = "InputLevels";
            this.InputLevels.Size = new System.Drawing.Size(44, 20);
            this.InputLevels.TabIndex = 3;
            this.InputLevels.Text = "7";
            // 
            // ButtonGenerate
            // 
            this.ButtonGenerate.Location = new System.Drawing.Point(7, 363);
            this.ButtonGenerate.Name = "ButtonGenerate";
            this.ButtonGenerate.Size = new System.Drawing.Size(138, 23);
            this.ButtonGenerate.TabIndex = 4;
            this.ButtonGenerate.Text = "Generate";
            this.ButtonGenerate.UseVisualStyleBackColor = true;
            this.ButtonGenerate.Click += new System.EventHandler(this.ButtonGenerate_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(14, 133);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(42, 13);
            this.label5.TabIndex = 10;
            this.label5.Text = "Output:";
            // 
            // OutputBase
            // 
            this.OutputBase.Location = new System.Drawing.Point(89, 148);
            this.OutputBase.MaxLength = 5;
            this.OutputBase.Name = "OutputBase";
            this.OutputBase.ReadOnly = true;
            this.OutputBase.Size = new System.Drawing.Size(44, 20);
            this.OutputBase.TabIndex = 13;
            this.OutputBase.TabStop = false;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(12, 151);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(69, 13);
            this.label7.TabIndex = 11;
            this.label7.Text = "• Value base";
            // 
            // OutputIncr
            // 
            this.OutputIncr.Location = new System.Drawing.Point(89, 174);
            this.OutputIncr.MaxLength = 5;
            this.OutputIncr.Name = "OutputIncr";
            this.OutputIncr.ReadOnly = true;
            this.OutputIncr.Size = new System.Drawing.Size(44, 20);
            this.OutputIncr.TabIndex = 14;
            this.OutputIncr.TabStop = false;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(12, 177);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(64, 13);
            this.label6.TabIndex = 12;
            this.label6.Text = "• Value Incr";
            // 
            // PanelTooltips
            // 
            this.PanelTooltips.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.PanelTooltips.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PanelTooltips.CausesValidation = false;
            this.PanelTooltips.Controls.Add(this.LabelTooltips);
            this.PanelTooltips.ForeColor = System.Drawing.Color.White;
            this.PanelTooltips.Location = new System.Drawing.Point(934, 21);
            this.PanelTooltips.Name = "PanelTooltips";
            this.PanelTooltips.Size = new System.Drawing.Size(200, 100);
            this.PanelTooltips.TabIndex = 15;
            this.PanelTooltips.Visible = false;
            // 
            // LabelTooltips
            // 
            this.LabelTooltips.AutoSize = true;
            this.LabelTooltips.Location = new System.Drawing.Point(7, 10);
            this.LabelTooltips.Name = "LabelTooltips";
            this.LabelTooltips.Size = new System.Drawing.Size(31, 13);
            this.LabelTooltips.TabIndex = 0;
            this.LabelTooltips.Text = "Hello";
            // 
            // GraphPanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1156, 398);
            this.Controls.Add(this.PanelTooltips);
            this.Controls.Add(this.OutputIncr);
            this.Controls.Add(this.OutputBase);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.ButtonGenerate);
            this.Controls.Add(this.InputLevels);
            this.Controls.Add(this.InputValueMax);
            this.Controls.Add(this.InputValueStart);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.PanelGraph);
            this.Controls.Add(this.FormulaList);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "GraphPanel";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "EBC Calculator";
            this.PanelGraph.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.PanelTooltips.ResumeLayout(false);
            this.PanelTooltips.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox FormulaList;
        private System.Windows.Forms.Panel PanelGraph;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox InputValueStart;
        private System.Windows.Forms.TextBox InputValueMax;
        private System.Windows.Forms.TextBox InputLevels;
        private System.Windows.Forms.Button ButtonGenerate;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox OutputBase;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox OutputIncr;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox InputVerticalNode;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox InputLevelJump;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel PanelTooltips;
        private System.Windows.Forms.Label LabelTooltips;
    }
}

