﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.VisualBasic.PowerPacks;

namespace EBC_Calculator
{
    public partial class GraphPanel : Form
    {
        const int MAX_LEVEL = 100;
        const int VERTICAL_NODES = 25;

        Color[] GraphColors = new Color[] { Color.Red, Color.Blue, Color.Green, Color.Yellow, Color.Purple, Color.Brown, Color.Orange};

        float LastResult;

        // Graph Components
        ShapeContainer graphContainer = new ShapeContainer();
        LineShape verticalLine = new LineShape();
        LineShape horizontalLine = new LineShape();
        List<LineShape> connectors = new List<LineShape>();
        List<OvalShape> nodes = new List<OvalShape>();
        List<OvalShape> dots = new List<OvalShape>();

        public GraphPanel()
        {
            InitializeComponent();
            DoubleBuffered = true;

            FormulaList.SelectedIndex = 0;
            InputLevels.KeyPress += NumberInputOnly;
            InputLevelJump.KeyPress += NumberInputOnly;
            InputVerticalNode.KeyPress += NumberInputOnly;
            InputValueMax.KeyPress += IPInputOnly;
            InputValueStart.KeyPress += IPInputOnly;

            // Init graph
            RecreateGraph();
            RegenerateDots();
        }

        void NumberInputOnly(Object sender, KeyPressEventArgs e)
        {
            e.Handled = !Char.IsNumber(e.KeyChar) && !Char.IsControl(e.KeyChar);
        }

        void IPInputOnly(Object sender, KeyPressEventArgs e)
        {
            e.Handled = !Char.IsNumber(e.KeyChar) && e.KeyChar != '.' && !Char.IsControl(e.KeyChar);
        }

        public void TestFormulaFormula(float value, int level)
        {
            LastResult = value + (float)Math.Pow(1.075f, value) * 1f * (30f - (30f * (value / 60f) * 0.5f)) * ((float)(level - 1) / 100f) + (0.01f * ((float)Math.Pow(1.05f, level) * level));
            //                                    a                b      c       c              d       e                                       f                       g
            // a: curvature. higher value makes level affect result more.
            // b: value multiplier
            // c: min value
            // d: max value
            // e: char level factor strength. higher value lower factor. 0 - 0.9
            // f: max result factor
            // g: curve strainess
        }

        public void LucidBeamFormula(float value, int level)
        {
            LastResult = value + (float)Math.Pow(1.008f, value) * 0.5f * (150f - (150f * (value / 300f) * 0f)) * ((float)(level - 1) / 100f) + (0.01f * ((float)Math.Pow(1.05f, level) * level));
        }

        public void DeathBlowFormula(float value, int level)
        {
            LastResult = value + (float)Math.Pow(1.07f, value) * (50f - (50f * (value / 100f) * 1f)) * ((float)(level - 1) / 100f) + (0.15f * ((float)Math.Pow(1.0005f, level) * level));
        }

        public void HealthPointFormula(float value, int level)
        {
            LastResult = value + (float)Math.Pow(1.1f, value * 0.125) * (125f - (125f * (value / 1000f) * 0.9f)) * ((float)(level - 1) / 100f) + (1350 * ((level - 1) / 100));
        }

        void RecreateGraph()
        {
            ClearGraph();
            graphContainer.Dispose();
            graphContainer = new ShapeContainer();
            graphContainer.Parent = PanelGraph;
            graphContainer.Visible = true;

            verticalLine.Dispose();
            verticalLine = new LineShape(graphContainer);
            verticalLine.X1 = 50;
            verticalLine.X2 = 50;
            verticalLine.Y1 = 15;
            verticalLine.Y2 = 315;

            horizontalLine.Dispose();
            horizontalLine = new LineShape(graphContainer);
            horizontalLine.X1 = 50;
            horizontalLine.X2 = 750;
            horizontalLine.Y1 = 315;
            horizontalLine.Y2 = 315;

            for (int i = 0; i < GraphColors.Length; i++)
            {
                RectangleShape shape = new RectangleShape(graphContainer);
                shape.BackStyle = BackStyle.Opaque;
                shape.FillStyle = FillStyle.Solid;
                shape.FillColor = GraphColors[i];
                shape.BorderColor = Color.Black;
                shape.Height = 10;
                shape.Width = 10;
                shape.Location = new Point(50 + 75 * i, 350);

                Label label = new Label();
                label.Parent = graphContainer;
                label.Location = new Point(shape.Location.X + 15, 350);
                label.AutoSize = true;
                label.Text = "Level " + (i + 1);
            }
        }

        void ClearGraph()
        {
            foreach (OvalShape shp in nodes)
                shp.Dispose();

            foreach (LineShape shp in connectors)
                shp.Dispose();

            nodes.Clear();
            connectors.Clear();
        }

        void RegenerateDots()
        {
            RecreateGraph();

            foreach (OvalShape shp in dots)
                shp.Dispose();
            dots.Clear();

            int level;
            if (InputLevelJump.Text.Length > 0)
                level = (MAX_LEVEL / int.Parse(InputLevelJump.Text));
            else
                level = 1;

            int offset = 700 / level;
            for (int i = 1; i <= level; i++)
            {
                OvalShape dot = new OvalShape(graphContainer);
                dot.BackStyle = BackStyle.Opaque;
                dot.FillStyle = FillStyle.Solid;
                dot.FillColor = Color.Blue;
                dot.BorderColor = Color.Blue;
                dot.Height = 2;
                dot.Width = 2;
                dot.Location = new Point(50 + offset * i, 314);
                dots.Add(dot);
            }

            int nodes;
            if (InputVerticalNode.Text.Length > 0)
                nodes = (int.Parse(InputVerticalNode.Text));
            else
                nodes = 1;
            offset = 300 / nodes;
            for (int i = 1; i <= nodes; i++)
            {
                OvalShape dot = new OvalShape(graphContainer);
                dot.BackStyle = BackStyle.Opaque;
                dot.FillStyle = FillStyle.Solid;
                dot.FillColor = Color.Blue;
                dot.BorderColor = Color.Blue;
                dot.Height = 2;
                dot.Width = 2;
                dot.Location = new Point(49, 315 - offset * i);
                dots.Add(dot);
            }
        }

        void Generate()
        {
            RegenerateDots();

            Type thisType = this.GetType();
            MethodInfo theMethod = thisType.GetMethod(FormulaList.Text + "Formula");

            int level = int.Parse(InputLevels.Text);
            float b = float.Parse(InputValueStart.Text);
            float m = float.Parse(InputValueMax.Text);
            float d = (m - b) / (level - 1);
            b -= d;

            int jump = int.Parse(InputLevelJump.Text);
            int levels;
            if (InputLevelJump.Text.Length > 0)
                levels = (MAX_LEVEL / jump);
            else
                levels = 1;

            int nodeCt;
            if (InputVerticalNode.Text.Length > 0)
                nodeCt = (int.Parse(InputVerticalNode.Text));
            else
                nodeCt = 1;
            
            int minX = 50, maxX = 750, minY = 315, maxY = 15;
            int dx = (maxX - minX) / levels, dy = (minY - maxY);

            float maxValue = 0;
            for (int j = level; j > 0; j--)
            {
                Point lastDot = Point.Empty;
                for (int i = levels; i > 0; i--)
                {
                    theMethod.Invoke(this, new object[] { b + d * j, i * jump });
                    float value = LastResult;
                    if (i == levels && j == level)
                        maxValue = value;

                    OvalShape dot = new OvalShape(graphContainer);
                    dot.BackStyle = BackStyle.Opaque;
                    dot.FillStyle = FillStyle.Solid;
                    dot.FillColor = GraphColors[j - 1];
                    dot.BorderColor = Color.Black;
                    dot.Height = 4;
                    dot.Width = 4;
                    dot.Location = new Point(minX + dx * i, minY - (int)(dy * (value / maxValue)));
                    dot.Name = "Skill level: " + j + "|Character level: " + (i * jump) + "|Value: " + value;
                    dot.MouseEnter += DotsMouseEnter;
                    dot.MouseLeave += DotsMouseLeave;
                    nodes.Add(dot);

                    if (i < levels)
                    {
                        LineShape line = new LineShape(graphContainer);
                        line.X1 = dot.Location.X + 2;
                        line.Y1 = dot.Location.Y + 2;
                        line.X2 = lastDot.X + 2;
                        line.Y2 = lastDot.Y + 2;
                        line.BorderWidth = 1;
                        line.BorderColor = dot.FillColor;
                        line.BorderStyle = System.Drawing.Drawing2D.DashStyle.Solid;
                        line.SendToBack();
                    }
                    lastDot = dot.Location;
                }
            }
        }

        private void FormulaList_SelectedIndexChanged(object sender, EventArgs e)
        {
            RegistryKey reg = Registry.CurrentUser.CreateSubKey("Software\\Verdant Atelier\\EmberCraft Calculator\\" + FormulaList.Text);
            InputValueStart.Text = reg.GetValue("Start Value", "0") as string;
            InputValueMax.Text = reg.GetValue("Max Value", "0") as string;
            InputLevels.Text = reg.GetValue("Levels", "7") as string;
            reg.Close();
        }

        private void ButtonGenerate_Click(object sender, EventArgs e)
        {
            // Save inputs
            RegistryKey reg = Registry.CurrentUser.CreateSubKey("Software\\Verdant Atelier\\EmberCraft Calculator\\" + FormulaList.Text);
            reg.SetValue("Start Value", InputValueStart.Text);
            reg.SetValue("Max Value", InputValueMax.Text);
            reg.SetValue("Levels", InputLevels.Text);
            reg.Close();

            // Print output
            int level = int.Parse(InputLevels.Text);
            float b = float.Parse(InputValueStart.Text);
            float m = float.Parse(InputValueMax.Text);
            float d = (m - b) / (level - 1);
            OutputBase.Text = ((b - d) + "").Replace(',', '.');
            OutputIncr.Text = (d + "").Replace(',', '.');

            ClearGraph();
            Generate();
        }

        private void InputLevelJump_TextChanged_1(object sender, EventArgs e)
        {
            RegenerateDots();
        }

        private void DotsMouseEnter(object sender, EventArgs e)
        {
            PanelTooltips.Show();
            PanelTooltips.Location = PointToClient(PanelGraph.PointToScreen((sender as OvalShape).Location));
            LabelTooltips.Text = (sender as OvalShape).Name.Replace("|", Environment.NewLine);
        }

        private void DotsMouseLeave(object sender, EventArgs e)
        {
            PanelTooltips.Hide();
        }
    }
}
